#include <timber/cli/Parameters.h>
#include <iostream>
#include <string>
#include <cassert>

using namespace timber::cli;

static Parameters makeParameters()
{
   Parameters opts;
   opts.addParameter(NoArgParameter< bool>("h", "help", "Help me", false, true, false));
   opts.addParameter(NoArgParameter< bool>("x", "", "Extract me", false, true, false));
   opts.addParameter(NoArgParameter< bool>("z", "", "Compress/Uncompress me", false, true, false));
   opts.addParameter(NoArgParameter< bool>("v", "", "Be verbose", false, true, false));
   opts.addParameter(ArgParameter< ::std::string>("f", "file", "the input", false));
   opts.addParameter(ArgParameter< ::std::string>("", "output", "the output", false));
   return opts;
}

void test_Parameters()
{
   char const* argv[] = { "1", "2", "3" };

   Parameters opts;
   ParsedArgs args = opts.parse(3, argv);
}

void test_normalizeArguments()
{
   Parameters opts = makeParameters();
   const char* argv[] = { "start", "-h", "--help", "-x", "-zvf-foo", "-f", "--bar", "--output=-bar", "free" };
   ::std::vector< ::std::string> args = opts.normalizeArguments(9, argv, 1);
   for (const ::std::string& arg : args) {
      ::std::cerr << '"' << arg << "\" ";
   }
   ::std::cerr << ::std::endl;
   assert(args.size() == 10);
   assert(args[0] == "-h");
   assert(args[1] == "-h");
   assert(args[2] == "-x");
   assert(args[3] == "-z");
   assert(args[4] == "-v");
   assert(args[5] == "-f-foo");
   assert(args[6] == "-f--bar");
   assert(args[7] == "--output=-bar");
   assert(args[8] == "--");
   assert(args[9] == "free");
}

void test_parse()
{
   Parameters opts;
   auto help = opts.addParameter(NoArgParameter< bool>("h", "help", "Help me", false, true, false));
   auto extract = opts.addParameter(NoArgParameter< bool>("x", "", "Extract me", false, true, false));
   auto gzip = opts.addParameter(NoArgParameter< bool>("z", "", "Compress/Uncompress me", false, true, false));
   auto bzip = opts.addParameter(NoArgParameter< bool>("j", "", "Compress/Uncompress me", false, true, false));
   auto verbose = opts.addParameter(NoArgParameter< bool>("v", "", "Be verbose", false, true, false));
   auto file = opts.addParameter(ArgParameter< ::std::string>("f", "file", "the input", false));
   auto output = opts.addParameter(ArgParameter< ::std::string>("\0", "output", "the output", false));

   const char* argv[] = { "start", "-h", "--help", "-x", "-zvvf-foo", "-f", "--bar", "--output=-bar", "free" };
   ParsedArgs args = opts.parse(9, argv, 1);
   assert(args.exists(gzip));
   assert(args.exists(file));
   ::std::cerr << "gzip = " << args.exists(gzip) << ::std::endl;

   assert(args.valueOf(file) == "-foo");
   assert(args.valueOf(gzip) == true);
   assert(args.valueOf(bzip) == false);
   assert(args.count(verbose) == 2);
   assert(args.count(file) == 2);
   assert(args.valuesOf(file)[0] == "-foo");
   assert(args.valuesOf(file)[1] == "--bar");
}

void test_emptyStringArg()
{
   Parameters opts;
   auto x = opts.addParameter(ArgParameter< ::std::string>("x", nullptr, "foo", false, ""));
   {
     ::std::cerr << "HERE" << ::std::endl;
     const char* argv[] = { "start", "-x", ""};
      ParsedArgs args = opts.parse(3, argv, 1);
      assert(args.exists(x));
      assert(args.count(x)==1);
      assert(args.valueOf(x) == "");
   }

   {
      const char* argv[] = { "start" };
      ParsedArgs args = opts.parse(1, argv, 1);
      assert(args.valueOf(x) == "");
      assert(!args.exists(x));
      assert(args.count(x)==0);
   }
}

void print_usage()
{
   makeParameters().usage(::std::cerr);
}

int main()
{
   test_emptyStringArg();
   test_parse();
   test_Parameters();
   test_normalizeArguments();
   print_usage();
   return 0;
}
