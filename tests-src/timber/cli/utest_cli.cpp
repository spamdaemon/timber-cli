#include <timber/cli/cli.h>
#include <iostream>
#include <cassert>
#include <string>

using namespace timber::cli;
using namespace ::std;

#define CHECK_PARAM(TYPE,VALUE) \
      ::std::cerr << #TYPE << ": " << ParseParameterText<TYPE>()(#VALUE) << ::std::endl;\
      assert(VALUE == ParseParameterText<TYPE>()(#VALUE))

void test1()
{
   CHECK_PARAM(int,-17);
   CHECK_PARAM(long,-17);
   CHECK_PARAM(long long,-17);
   CHECK_PARAM(unsigned long,17);
   CHECK_PARAM(unsigned long long,17);
   CHECK_PARAM(long double,17.25);
   CHECK_PARAM(double,17.25);
   CHECK_PARAM(float,17.25);

   assert("12345d" == ParseParameterText<string>()("12345d"));
   assert(ParseParameterText<::std::u32string>()( u8"あか").length()==2);
}

int main()
{
   test1();
   return 0;
}
