#include <timber/cli/ArgParameter.h>
#include <cassert>

using namespace timber::cli;

enum class Color
{
   RED, GREEN, YELLOW
};

static Color parseColor(const ::std::string& text)
{
   if (text == "RED") {
      return Color::RED;
   }
   if (text == "GREEN") {
      return Color::GREEN;
   }
   if (text == "YELLOW") {
      return Color::YELLOW;
   }
   throw ::std::runtime_error("Invalid color");
}

void test1()
{
   ArgParameter< int> foo("o", "output", "Description", false, "3");
   assert(foo.description() == "Description");
   assert(foo.shortParameter() == "o");
   assert(foo.longParameter() == "output");
   assert(foo.isRequired() == false);
   assert(foo.valueOf("2") == 2);
}

void test2()
{
   ArgParameter< Color> foo("c", "color", "The text color", true, nullptr, parseColor);
   ArgParameter< Color> bar("c", "color", "The text color", true, nullptr, parseColor);
   assert(bar.description() == foo.description());
   assert(bar.shortParameter() == foo.shortParameter());
   assert(bar.longParameter() == foo.longParameter());
   assert(bar.isRequired() == foo.isRequired());
   assert(bar.unspecifiedValue() == foo.unspecifiedValue());
}

void test3()
{
   ArgParameter< int> foo("o", "output", "Description", false, "3");
   // verify default value
   assert(foo.valueOf(nullptr) == 3);
   // verify that we throw an exception if input is invalid
   try {
      foo.valueOf("a");
      assert(false);
   }
   catch (const ::std::exception&) {
   }
}

void test4()
{
   // constructor needs to throw, if default value is invalid
   try {
      ArgParameter< int> foo("o", "output", "Description", false, "a");
      assert(false);
   }
   catch (...) {

   }
}

void test5()
{
   // constructor needs to throw, if default value is invalid
   try {
      ArgParameter< ::std::string> foo("o", "output", "Description", false, "");
   }
   catch (...) {
      assert(false);
   }
}

int main()
{
   test1();
   test2();
   test3();
   test4();
   test5();
   return 0;
}
