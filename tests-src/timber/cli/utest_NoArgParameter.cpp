#include <timber/cli/NoArgParameter.h>
#include <cassert>

using namespace timber::cli;

enum class Color
{
   RED, GREEN, YELLOW
};

void test1()
{
   NoArgParameter< Color> foo("c", "color", "Color", true, Color::RED, Color::GREEN);
   assert(foo.description() == "Color");
   assert(foo.shortParameter() == "c");
   assert(foo.longParameter() == "color");
   assert(foo.isRequired() == true);
   assert(foo.specifiedValue() == Color::RED);
   assert(*foo.unspecifiedValue() == Color::GREEN);
}

void test2()
{
   NoArgParameter< Color> foo("c", "color", "Color", true, Color::RED);
   assert(foo.description() == "Color");
   assert(foo.shortParameter() == "c");
   assert(foo.longParameter() == "color");
   assert(foo.isRequired() == true);
   assert(foo.specifiedValue() == Color::RED);
   assert(foo.unspecifiedValue() == nullptr);
}

void test3()
{
   NoArgParameter< Color> foo("c", "color", "Color", true, Color::RED, Color::YELLOW);
   NoArgParameter< Color> bar(foo);

   assert(bar.description() == foo.description());
   assert(bar.shortParameter() == foo.shortParameter());
   assert(bar.longParameter() == foo.longParameter());
   assert(bar.isRequired() == foo.isRequired());
   assert(bar.specifiedValue() == foo.specifiedValue());
   assert(bar.unspecifiedValue() == foo.unspecifiedValue());
}

void test4()
{
   NoArgParameter< Color> foo(u8"赤", u8"あか", u8"色", true, Color::RED, Color::YELLOW);
}

int main()
{
   test1();
   test2();
   test3();
   test4();
   return 0;
}
