#ifndef _TIMBER_CLI_PARAMETERS_H
#define _TIMBER_CLI_PARAMETERS_H

#ifndef _TIMBER_CLI_PARAMETERBASE_H
#include <timber/cli/ParameterBase.h>
#endif

#ifndef _TIMBER_CLI_ARGPARAMETER_H
#include <timber/cli/ArgParameter.h>
#endif

#ifndef _TIMBER_CLI_NOARGPARAMETER_H
#include <timber/cli/NoArgParameter.h>
#endif

#ifndef _TIMBER_CLI_PARSEDARGS_H
#include <timber/cli/ParsedArgs.h>
#endif

#ifndef _TIMBER_CLI_PARAMETERARGUMENT_H
#include <timber/cli/ParameterArgument.h>
#endif

#include <string>
#include <ostream>
#include <memory>
#include <vector>
#include <map>
#include <stdexcept>
#include <functional>

namespace timber {
   namespace cli {

      /**
       */
      class Parameters
      {
            /** A pointer */
         public:
            typedef ::std::shared_ptr< ParameterBase> ParameterPtr;

            /** A pointer */
         public:
            typedef ::std::shared_ptr< ParameterArgumentBase> ParameterValuePtr;

            /**
             * A constructor function
             */
         private:
            typedef ::std::function< ParameterValuePtr(const ParameterBase&, const char*)> ParameterValueConstructor;

            /**
             * Default constructor.
             */
         public:
            Parameters();

            /**
             * Copy constructor
             * @param src
             */
         public:
            Parameters(const Parameters& src);

            /**
             * Destructor.
             */
         public:
            virtual ~Parameters();

            /**
             * Print usage information to the given output stream.
             * @param stream an output stream
             */
         public:
            void usage(::std::ostream& stream) const;

            /**
             * Add a new parameter.
             * @param opt an unparsed parameter
             * @return the a copy of the parameter as a shared pointer.
             * @throws ::std::exception if the parameter conflicts with an existing parameter
             */
         public:
            template<class T>
            inline ::std::shared_ptr< NoArgParameter< T>> addParameter(const NoArgParameter< T>& opt)
            {
               ParameterValueConstructor ctor = createNoArgParameterValue< T>;
               ::std::shared_ptr< NoArgParameter< T>> ptr(new NoArgParameter< T>(opt));
               insertParameter(ptr, ctor);
               return ptr;
            }

            /**
             * Add a new parameter.
             * @param opt an unparsed parameter
             * @return the a copy of the parameter as a shared pointer.
             * @throws ::std::exception if the parameter conflicts with an existing parameter
             */
         public:
            template<class T>
            inline ::std::shared_ptr< ArgParameter< T>> addParameter(const ArgParameter< T>& opt)
            {
               ParameterValueConstructor ctor = createArgParameterValue< T>;
               ::std::shared_ptr< ArgParameter< T>> ptr(new ArgParameter< T>(opt));
               insertParameter(ptr, ctor);
               return ptr;
            }

            /**
             * Process the arguments into a normalized vector of arguments. Each entry in the vector will be either
             * an parameter, an parameter argument, or a non-parameter value.
             * @param argc the number of arguments to process
             * @param argv the argument vector
             * @param offset the offset of the first argument to be processed.
             * @throws ::std::exception if an unknown parameter is encountered
             */
         public:
            ::std::vector< ::std::string> normalizeArguments(size_t argc, const char** argv, size_t offset) const;

            /**
             * Parse the specified arguments.
             * @param argc the argument count
             * @param argv the argument vector
             * @param offset the first argument offset
             * @return parsed arguments
             * @throws ::std::exception on error
             */
         public:
            ParsedArgs parse(size_t argc, const char** argv, size_t offset = 1) const;

            /**
             * Find the parameter that corresponds to the specified string.
             * @param opt a candidate parameter
             * @parma parameter the parameter that was parsed out and corresponds to a long or short parameter
             * @param value the value of the parameter (will be preceeded by an equal)
             * @param shortOpts the remaining short opts of the string, after parsing
             * @return the parameter that was parsed or null if opt is not an parameter
             * @throws exception if the string is an parameter but was not found
             */
         private:
            ParameterPtr findParameter(const ::std::string& opt, ::std::string& parameter, ::std::string& value,
                  ::std::string& shortOpts) const;

            /**
             * Insert a new parameter.
             * @param opt the new parameter to add
             * @param ctor the constructor
             * @return the parameter pointer
             */
         private:
            void insertParameter(ParameterPtr opt, ParameterValueConstructor ctor);

         private:
            template<class T>
            static ::std::shared_ptr< ParameterArgumentBase> createArgParameterValue(const ParameterBase& xopt,
                  const char* value)
            {
               const ArgParameter< T>& opt = dynamic_cast< const ArgParameter< T>&>(xopt);
               return ::std::shared_ptr< ParameterArgumentBase>(new ParameterArgument< T>(opt.valueOf(value)));
            }

         private:
            template<class T>
            static ::std::shared_ptr< ParameterArgumentBase> createNoArgParameterValue(const ParameterBase& xopt,
                  const char* value)
            {
               if (value != nullptr) {
                  throw ::std::invalid_argument("No argument expected");
               }
               const NoArgParameter< T>& opt = dynamic_cast< const NoArgParameter< T>&>(xopt);
               return ::std::shared_ptr< ParameterArgumentBase>(new ParameterArgument< T>(opt.specifiedValue()));
            }

            /** The known parameters */
         private:
            ::std::map< ParameterPtr, ParameterValueConstructor> _parameters;
      };
   }
}

#endif
