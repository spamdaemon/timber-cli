#include <timber/cli/cli.h>
#include <codecvt>
#include <locale>

namespace timber {
   namespace cli {

      ::std::u32string ParseParameterText< ::std::u32string>::operator()(const ::std::string& text) const
      {
         std::wstring_convert< std::codecvt_utf8< char32_t>, char32_t> ucs2conv;
         try {
            return ucs2conv.from_bytes(text);
         }
         catch (const ::std::range_error&) {
            // the conversion failed
            throw ::std::invalid_argument("Not a valid utf8 string");
         }
      }

      ::std::string ParseParameterText< ::std::string>::operator()(::std::string text) const throw()
      {
         return text;
      }

      int ParseParameterText< int>::operator()(const ::std::string& text) const
      {
         size_t pos;
         int res = ::std::stoi(text, &pos);
         if (pos == 0 || pos != text.length()) {
            throw ::std::runtime_error("Failed to parse integer");
         }
         return res;
      }

      long ParseParameterText< long>::operator()(const ::std::string& text) const
      {
         size_t pos;
         long res = ::std::stol(text, &pos);
         if (pos == 0 || pos != text.length()) {
            throw ::std::runtime_error("Failed to parse long");
         }
         return res;
      }
      long long ParseParameterText< long long>::operator()(const ::std::string& text) const
      {
         size_t pos;
         long long res = ::std::stoll(text, &pos);
         if (pos == 0 || pos != text.length()) {
            throw ::std::runtime_error("Failed to parse long long");
         }
         return res;
      }
      unsigned long ParseParameterText< unsigned long>::operator()(const ::std::string& text) const
      {
         size_t pos;
         unsigned long res = ::std::stoul(text, &pos);
         if (pos == 0 || pos != text.length()) {
            throw ::std::runtime_error("Failed to parse unsigned long");
         }
         return res;
      }

      unsigned long long ParseParameterText< unsigned long long>::operator()(const ::std::string& text) const
      {
         size_t pos;
         unsigned long long res = ::std::stoull(text, &pos);
         if (pos == 0 || pos != text.length()) {
            throw ::std::runtime_error("Failed to parse unsigned long long");
         }
         return res;
      }

      float ParseParameterText< float>::operator()(const ::std::string& text) const
      {
         size_t pos;
         float res = ::std::stof(text, &pos);
         if (pos == 0 || pos != text.length()) {
            throw ::std::runtime_error("Failed to parse float");
         }
         return res;
      }
      double ParseParameterText< double>::operator()(const ::std::string& text) const
      {
         size_t pos;
         double res = ::std::stod(text, &pos);
         if (pos == 0 || pos != text.length()) {
            throw ::std::runtime_error("Failed to parse double");
         }
         return res;
      }
      long double ParseParameterText< long double>::operator()(const ::std::string& text) const
      {
         size_t pos;
         long double res = ::std::stold(text, &pos);
         if (pos == 0 || pos != text.length()) {
            throw ::std::runtime_error("Failed to parse long double");
         }
         return res;
      }
   }
}

