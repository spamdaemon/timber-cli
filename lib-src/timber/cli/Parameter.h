#ifndef _TIMBER_CLI_PARAMETER_H
#define _TIMBER_CLI_PARAMETER_H

#ifndef _TIMBER_CLI_PARAMETERBASE_H
#include <timber/cli/ParameterBase.h>
#endif

namespace timber {
   namespace cli {

      template<class T>
      class Parameter : public ParameterBase
      {
         private:
            Parameter() = delete;

            /**
             * Copy constructor.
             * @param src the source object
             */
         public:
            Parameter(char const* sOpt, char const* lOpt = nullptr, const char* desc = nullptr, bool required = false)
                  : ParameterBase(sOpt, lOpt, desc, required)
            {
            }

            /**
             * Copy constructor.
             * @param opt the source object
             */
         public:
            Parameter(const Parameter& opt)
                  : ParameterBase(opt)
            {
            }

         public:
            ~Parameter() 
            {
            }
      };
   }
}

#endif
