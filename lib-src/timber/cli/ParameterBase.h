#ifndef _TIMBER_CLI_PARAMETERBASE_H
#define _TIMBER_CLI_PARAMETERBASE_H

#include <string>

namespace timber {
   namespace cli {

      class ParameterBase
      {
         private:
            ParameterBase() = delete;

            /**
             * A copy constructor.
             */
         public:
            ParameterBase(const ParameterBase& opt) ;

            /**
             * Create a required parameter. The short parameters must be a single unicode codepoint!
             * @param sOpt the short parameter (use <em>-</em> or <em>\0</em> if there is no short parameter)
             * @param lOpt the long parameter (can be nullptr or empty string)
             * @param desc the description
             * @param required true if this parameter is required
             * @throws invalid_argument if neither a short, nor a long parameter are specified
             * @throws invalid_argument if long parameter contains a <em>=</em> or <em>-</em>
             */
         public:
            ParameterBase(char const* sOpt, char const* lOpt = nullptr, const char* desc = nullptr,
                  bool required = false) ;

            /**
             * Destructor
             */
         public:
            virtual ~ParameterBase()  =0;

            /**
             * Get a name for this parameter.
             * @return a name
             */
         public:
            ::std::string name() const ;

            /**
             * The parameter character string. If no short parameter is known, then an empty string is returned.
             * @return the short parameter string
             */
         public:
            const ::std::string& shortParameter() const ;

            /**
             * The parameter character string. If no long parameter is known, then an empty string is returned.
             * @return the short parameter string
             */
         public:
            const ::std::string& longParameter() const ;

            /**
             * Is this parameter required. If an parameter is required, then defaultValue() throwws an exception.
             * @return true if this parameter is required, false otherwise
             */
         public:
            bool isRequired() const ;

            /**
             * Get the description for this parameter.
             * @return the description for this parameter
             */
         public:
            const ::std::string& description() const ;

            /**
             * Determine if this parameter requires an argument.
             * @return true if this parameter requires an argument
             */
         public:
            virtual bool argumentRequired() const  = 0;

            /** The short parameter string */
         private:
            ::std::string _shortOpt;

            /** The long parameter string */
         private:
            ::std::string _longOpt;

            /** The description */
         private:
            ::std::string _description;

            /** True if this parameter is required */
         private:
            bool _required;
      };
   }
}

#endif
