#ifndef _TIMBER_CLI_PARSEDARGS_H
#define _TIMBER_CLI_PARSEDARGS_H

#ifndef _TIMBER_CLI_PARAMETERBASE_H
#include <timber/cli/ParameterBase.h>
#endif

#ifndef _TIMBER_CLI_ARGPARAMETER_H
#include <timber/cli/ArgParameter.h>
#endif

#ifndef _TIMBER_CLI_NOARGPARAMETER_H
#include <timber/cli/NoArgParameter.h>
#endif

#ifndef _TIMBER_CLI_PARAMETERARGUMENT_H
#include <timber/cli/ParameterArgument.h>
#endif

#include <map>
#include <vector>
#include <string>
#include <memory>

namespace timber {
   namespace cli {
      class Parameters;

      /**
       * The parsed arguments.
       */
      class ParsedArgs
      {
            friend class Parameters;

            /** A pointer */
         public:
            typedef ::std::shared_ptr< ParameterBase> ParameterPtr;

            /** A value pointer */
         private:
            typedef ::std::shared_ptr< ParameterArgumentBase> ValuePtr;

            /**
             * Default constructor.
             */
         public:
            ParsedArgs() ;

            /**
             * Create a parsed args object.
             * @param optStrings the parsed parameter strings
             * @param freeArgs free arguments
             * @throws ::std::exception on error
             */
         private:
            ParsedArgs(::std::map< ParameterPtr, ::std::vector< ValuePtr>> &&optStrings,
                  ::std::vector< ::std::string>&& freeArgs) ;

            /**
             * Destructor
             */
         public:
            ~ParsedArgs() throw();

            /**
             * Get the unconsumed or free arguments in the originally
             * specified order.
             * @return an array of argument strings
             */
         public:
            inline const ::std::vector< ::std::string>& freeArguments() const 
            {
               return _freeArgs;
            }

            /**
             * Get the value of the specified parameter.
             * @param ptr a parameter pointer
             * @return true if the parameter was specified, false otherwise
             */
         public:
            bool exists(const ParameterPtr& ptr) const ;

            /**
             * Get the number of times an parameter was specified.
             * @param opt an parameter
             * @return the number of times the parameter was specified
             */
         public:
            size_t count(const ParameterPtr& ptr) const ;

            /**
             * Get all values of the specified parameters.
             * @return opt an parameter
             * @return the value of the parameter
             */
         public:
            template<class T>
            ::std::vector< T> valuesOf(const ::std::shared_ptr< ArgParameter< T>>& opt) const 
            {
               ::std::vector< T> result;
               auto i = _optStrings.find(opt);
               if (i != _optStrings.end()) {
                  for (auto ptr : i->second) {
                     result.push_back(valueOf< T>(ptr));
                  }
               }
               return result;
            }

            /**
             * Get the first (or only value of the specified parameter)
             * @param opt an parameter requiring an argument
             * @see valuesOf
             */
         public:
            template<class T>
            T valueOf(const ::std::shared_ptr< ArgParameter< T>>& opt) const 
            {
               auto i = _optStrings.find(opt);
               if (i == _optStrings.end()) {
                  return opt->valueOf(nullptr);
               }
               return valueOf< T>(i->second[0]);
            }

            /**
             * Get the first (or only value of the specified parameter)
             * @param opt an parameter requiring an argument
             */
         public:
            template<class T>
            T valueOf(const ::std::shared_ptr< NoArgParameter< T>>& opt) const 
            {
               auto i = _optStrings.find(opt);
               if (i == _optStrings.end()) {
                  auto x = opt->unspecifiedValue();
                  if (!x) {
                     throw ::std::invalid_argument("No default exists for parameter " + opt->name());
                  }
                  return *x;
               }
               return valueOf< T>(i->second[0]);
            }

            /**
             * Cast an parameter as a type.
             * @param ptr a value pointer
             * @return the value as the specified type
             */
         private:
            template<class T>
            static const T& valueOf(const ValuePtr& ptr) 
            {
               return ::std::dynamic_pointer_cast< ParameterArgument< T>>(ptr)->value();
            }

            /** The parameters and their string values */
         private:
            ::std::map< ParameterPtr, ::std::vector< ValuePtr>> _optStrings;

            /** The free arguments */
         private:
            ::std::vector< ::std::string> _freeArgs;

            /** An empty string */
         private:
            ::std::string _empty;
      };
   }
}

#endif
