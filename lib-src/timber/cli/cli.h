#ifndef _TIMBER_CLI_H
#define _TIMBER_CLI_H

#include <string>
#include <sstream>
#include <stdexcept>

namespace timber {
   namespace cli {

      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<class T>
      struct ParseParameterText
      {
            inline T operator()(const ::std::string& text) const 
            {
               if (text.empty()) {
                  throw ::std::runtime_error("Cannot convert empty text");
               }
               ::std::istringstream stream(text);
               T value;
               stream >> value;
               if (stream.fail()) {
                  throw ::std::runtime_error("Failed to parse");
               }
               // the entire text string needs to be consumed in order for a parse to be
               // considered successful
               if (!stream.eof()) {
                  throw ::std::runtime_error("Failed to parse entire text");
               }
               return value;
            }
      };

      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< ::std::string>
      {
            ::std::string operator()(::std::string text) const throw();
      };

      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< ::std::u32string>
      {
            ::std::u32string operator()(const ::std::string& text) const;
      };

      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< int>
      {
            int operator()(const ::std::string& text) const ;
      };

      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< long>
      {
            long operator()(const ::std::string& text) const ;
      };
      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< long long>
      {
            long long operator()(const ::std::string& text) const ;
      };
      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< unsigned long>
      {
            unsigned long operator()(const ::std::string& text) const ;
      };
      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< unsigned long long>
      {
            unsigned long long operator()(const ::std::string& text) const ;
      };

      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< float>
      {
            float operator()(const ::std::string& text) const ;
      };

      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< double>
      {
            double operator()(const ::std::string& text) const ;
      };

      /**
       * Parse a text value into a given type.
       * @param text
       * @return a type
       */
      template<>
      struct ParseParameterText< long double>
      {
            long double operator()(const ::std::string& text) const ;
      };
   }
}

#endif
