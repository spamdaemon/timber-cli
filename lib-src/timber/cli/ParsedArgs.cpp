#include <timber/cli/ParsedArgs.h>

namespace timber {
   namespace cli {

      ParsedArgs::ParsedArgs() 
      {
      }

      ParsedArgs::ParsedArgs(::std::map< ParameterPtr, ::std::vector< ValuePtr>>&& optStrings,
            ::std::vector< ::std::string>&& freeArgs)
            
            : _optStrings(::std::move(optStrings)), _freeArgs(::std::move(freeArgs))
      {

      }

      ParsedArgs::~ParsedArgs() throw()
      {
      }

      bool ParsedArgs::exists(const ParameterPtr& ptr) const 
      {
         return _optStrings.find(ptr) != _optStrings.end();
      }

      size_t ParsedArgs::count(const ParameterPtr& ptr) const 
      {
         auto i = _optStrings.find(ptr);
         if (i == _optStrings.end()) {
            return 0;
         }
         return i->second.size();
      }

   }
}
