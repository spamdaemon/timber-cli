#ifndef _TIMBER_CLI_PARAMETERARGUMENTBASE_H
#define _TIMBER_CLI_PARAMETERARGUMENTBASE_H

namespace timber {
   namespace cli {

      /**
       * A simple base class for the various kinds of parameter arguments.
       */
      class ParameterArgumentBase
      {
            /**
             * Constructor
             */
         protected:
            ParameterArgumentBase() throw();

            /**
             * Destructor
             */
         public:
            virtual ~ParameterArgumentBase() throw() = 0;

      };

   }
}
#endif
