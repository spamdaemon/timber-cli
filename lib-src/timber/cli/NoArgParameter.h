#ifndef _TIMBER_CLI_NOARGPARAMETER_H
#define _TIMBER_CLI_NOARGPARAMETER_H

#ifndef _TIMBER_CLI_PARAMETER_H
#include <timber/cli/Parameter.h>
#endif

#include <string>
#include <memory>
#include <functional>

namespace timber {
   namespace cli {

      template<class T> class NoArgParameter : public Parameter< T>
      {
            /**
             * A function that can be provided to provide customized parsing for an parameter.
             */
         public:
            typedef ::std::function< T(const ::std::string&)> Parser;

         private:
            NoArgParameter() = delete;

            /**
             * Copy constructor.
             * @param src the source object
             */
         public:
            NoArgParameter(const NoArgParameter& src)
                  : Parameter< T>(src), _ifSpecified(src._ifSpecified), _ifNotSpecified(src._ifNotSpecified)
            {
            }

         public:
            ~NoArgParameter()
            {
            }

            /**
             * Create an parameteral parameter for which a default value must be specified.
             * @param sOpt the short parameter (if '\0')
             * @param lOpt the long parameter (can be nullptr)
             * @param defValue the default value
             * @param desc the description
             */
         public:
            NoArgParameter( char const* sOpt, char const* lOpt, const char* desc, bool required, const T& specified,
                  const T& unspecified)
                  : Parameter< T>(sOpt, lOpt, desc, required), _ifSpecified(specified), _ifNotSpecified(new T(unspecified))
            {
            }

            /**
             * Create an parameteral parameter for which a default value must be specified.
             * @param sOpt the short parameter (if '\0')
             * @param lOpt the long parameter (can be nullptr)
             * @param defValue the default value
             * @param desc the description
             */
         public:
            NoArgParameter( char const* sOpt, char const* lOpt, const char* desc, bool required, const T& specified,
                  ::std::unique_ptr< T> unspecified = nullptr)
                  : Parameter< T>(sOpt, lOpt, desc, required), _ifSpecified(specified), _ifNotSpecified(
                        ::std::move(unspecified))
            {
            }

         public:
            bool argumentRequired() const 
            {
               return false;
            }

            /**
             * Get the value if specified
             * @return the value when the object is specified
             */
         public:
            inline const T& specifiedValue() const 
            {
               return _ifSpecified;
            }

            /**
             * Get the value if not specified.
             * @return the value when the object is not specified
             */
         public:
            inline const ::std::shared_ptr< T> unspecifiedValue() const 
            {
               return _ifNotSpecified;
            }

            /** The value, if this parameter was specified */
         private:
            T _ifSpecified;

            /** The value, if this parameter was not specified */
         private:
            ::std::shared_ptr< T> _ifNotSpecified;
      };
   }
}

#endif
