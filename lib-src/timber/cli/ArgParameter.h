#ifndef _TIMBER_CLI_ARGPARAMETER_H
#define _TIMBER_CLI_ARGPARAMETER_H

#ifndef _TIMBER_CLI_H
#include <timber/cli/cli.h>
#endif

#ifndef _TIMBER_CLI_PARAMETER_H
#include <timber/cli/Parameter.h>
#endif

#include <string>
#include <memory>
#include <functional>
#include <stdexcept>

namespace timber {
   namespace cli {

      template<class T>
      class ArgParameter : public Parameter< T>
      {
            /**
             * A function that can be provided to provide customized parsing for an parameter.
             */
         public:
            typedef ::std::function< T(const ::std::string&)> Parser;

         private:
            ArgParameter() = delete;

            /**
             * Copy constructor.
             * @param src the source object
             */
         public:
            ArgParameter(const ArgParameter& src)
                  : Parameter< T>(src), _ifNotSpecified(src._ifNotSpecified), _valueIfNotSpecified(
                        src._valueIfNotSpecified), _parser(src._parser)
            {
            }

            /**
             * Create an parameteral parameter for which a default value must be specified.
             * @param sOpt the short parameter (if '\0')
             * @param lOpt the long parameter (can be nullptr)
             * @param defValue the default value
             * @param desc the description
             * @throws ::std::exception if the default value text cannot be parsed
             */
         public:
            ArgParameter(char const* sOpt, char const* lOpt, const char* desc, bool required, const char* unspecified = nullptr,
                  Parser parseFN = Parser(ParseParameterText< T>()))
                  : Parameter< T>(sOpt, lOpt, desc, required), _parser(parseFN)
            {
               if (unspecified) {
                  _ifNotSpecified = unspecified;
                  _valueIfNotSpecified.reset(new T(parseFN(_ifNotSpecified)));
               }
            }

         public:
            ~ArgParameter()
            {
            }

         public:
            bool argumentRequired() const
            {
               return true;
            }

            /**
             * Get teh default value.
             * @return the default value.
             */
         public:
            inline ::std::shared_ptr< T> unspecifiedValue() const
            {
               return _valueIfNotSpecified;
            }

            /**
             * Get the value of this parameter.
             * @param arg the parameter value or nullptr if not specified
             * @return a value
             * @throws ::std::invalid_argument if the arg==nullptr and this parameter has no default
             * @throws ::std::invalid_argument if the arg is a string, but cannot be parsed
             */
         public:
            T valueOf(const char* arg) const
            {
               if (arg == nullptr) {
                  if (_valueIfNotSpecified) {
                     return *_valueIfNotSpecified;
                  }
                  throw ::std::invalid_argument("Parameter "+ParameterBase::name()+": missing argument");
               }
               try {
                  return _parser(arg);
               }
               catch (::std::exception&) {
                  throw ::std::invalid_argument("Parameter "+ParameterBase::name()+": invalid argument" + arg);
               }
            }

            /** The unspecified value */
         private:
            ::std::string _ifNotSpecified;

            /** The default value */
         private:
            ::std::shared_ptr< T> _valueIfNotSpecified;

            /** A parser */
         private:
            Parser _parser;
      };
   }
}

#endif
