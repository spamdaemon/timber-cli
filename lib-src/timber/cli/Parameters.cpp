#include <timber/cli/Parameters.h>
#include <map>
#include <cassert>
#include <iostream>

namespace timber {
   namespace cli {

      namespace {

         enum ParameterType
         {
            LONG, SHORT, NONE
         };

         ::std::string splitShortParameter(const ::std::string& opt, ::std::string& remainder)
         {
            if (opt.empty() || opt[0]!='-') {
               throw ::std::invalid_argument("Invalid short option '"+opt+"'");
            }
            // parameter starts with --
            ::std::string parameter(1, opt[1]);
            remainder = opt.substr(2);
            return parameter;
         }

         ::std::string splitLongParameter(const ::std::string& opt, ::std::string& remainder)
         {
            // parameter starts with --
            if (opt.length()<=2 || opt[0]!='-' || opt[1]!='-') {
               throw ::std::invalid_argument("Invalid long option '"+opt+"'");
            }
            const ::std::string::size_type i = 2;
            const ::std::string::size_type j = opt.find_first_of('=');
            ::std::string parameter;
            if (j != ::std::string::npos) {
               parameter = opt.substr(i, j - i);
               remainder = opt.substr(j);
            }
            else {
               remainder.clear();
               parameter = opt.substr(i);
            }
            if (!remainder.empty() && remainder[0]!='=') {
               throw ::std::invalid_argument("Invalid long option '"+opt+"'");
            }

            return parameter;
         }
      }

      Parameters::Parameters() 
      {
      }

      Parameters::~Parameters() 
      {
      }

      void Parameters::usage(::std::ostream& stream) const
      {
         ::std::map< ::std::string, ParameterPtr> parameters;
         for (auto i = _parameters.begin(); i != _parameters.end(); ++i) {
            parameters[i->first->name()] = i->first;
         }
         stream << "Parameters:" << ::std::endl;
         for (auto i = parameters.begin(); i != parameters.end(); ++i) {
            const ParameterBase& opt = *i->second;
            ::std::string sep = " ";
            if (!opt.shortParameter().empty()) {
               stream << sep << "-" << opt.shortParameter();
               sep = " , ";
            }
            if (!opt.longParameter().empty()) {
               stream << sep << "--" << opt.longParameter();
            }
            stream << "\t" << opt.description();
            if (opt.isRequired()) {
               stream << " [ REQUIRED ]";
            }
            stream << ::std::endl;
         }
      }

      void Parameters::insertParameter(ParameterPtr opt, ParameterValueConstructor ctor) 
      {
         for (auto i = _parameters.begin(); i != _parameters.end(); ++i) {
            const ParameterPtr& p = i->first;
            if (!opt->shortParameter().empty() && p->shortParameter() == opt->shortParameter()) {
               throw ::std::invalid_argument("Duplicate short parameter");
            }
            if (!opt->longParameter().empty() && p->longParameter() == opt->longParameter()) {
               throw ::std::invalid_argument("Duplicate long parameter");
            }
         }
         _parameters[opt] = ctor;
      }

      Parameters::ParameterPtr Parameters::findParameter(const ::std::string& opt, ::std::string& parameter, ::std::string& value,
            ::std::string& shortOpts) const 
      {
         value.clear();
         ParameterType type = NONE;
         parameter.clear();
         ::std::string remainder;
         if (opt.length() > 1 && opt[0] == '-') {
            if (opt.length() > 2 && opt[1] == '-') {
               parameter = splitLongParameter(opt, remainder);
               type = LONG;
            }
            else {
               parameter = splitShortParameter(opt, remainder);
               type = SHORT;
            }
         }
         if (type == NONE) {
            return nullptr;
         }
         for (auto i = _parameters.begin(); i != _parameters.end(); ++i) {
            const ParameterPtr& o = i->first;
            if (type == LONG && o->longParameter() == parameter) {
               shortOpts.clear();
               if (o->argumentRequired()) {
                  value = ::std::move(remainder);
               }
               else {
                  value.clear();
               }
               return o;
            }
            else if (type == SHORT && o->shortParameter() == parameter) {
               if (o->argumentRequired()) {
                  if (remainder.empty()) {
                     value.clear();
                  }
                  else {
                     value = "=" + remainder;
                  }
                  shortOpts.clear();
               }
               else {
                  value.clear();
                  shortOpts = remainder;
               }
               return o;
            }
         }
         throw ::std::invalid_argument("Unknown parameter " + parameter);
      }

      ::std::vector< ::std::string> Parameters::normalizeArguments(size_t argc, const char** argv,
            size_t offset) const 
      {
         ::std::vector< ::std::string> args;
         ::std::string arg;
         bool haveArg = false;
         bool argStopFound = false;
         for (size_t i = offset; i < argc;) {
            if (!haveArg) {
               arg = argv[i];
            }
            haveArg = false;

            // if we've encountered "--" now, or in the past
            // then copy the remaining arguments over
            if (argStopFound || arg == "--") {
               argStopFound = true;
               args.push_back(arg);
               ++i;
               continue;
            }

            ::std::string parameter, value, shortOpts;
            // if we find something that is not an parameter, then
            // treat as having processed all parameters, just as if we've found a argStop
            ParameterPtr opt = findParameter(arg, parameter, value, shortOpts);
            assert(value.empty() || value[0] == '=');
            if (!opt) {
               args.push_back("--");
               argStopFound = true;
               args.push_back(arg);
               ++i;
               continue;
            }

            ::std::string normalizedParameter;

            // if the parameter takes an argument, we use either the remainder
            if (!opt->shortParameter().empty()) {
               normalizedParameter = "-" + opt->shortParameter();
            }
            else {
               normalizedParameter = "--" + opt->longParameter() + "=";
            }

            // now, deal with arguments
            if (opt->argumentRequired()) {
               if (value.empty()) {
                  if (++i < argc) {
                     normalizedParameter += argv[i];
                  }
                  else {
                     throw ::std::invalid_argument("Expected an parameter argument");
                  }
               }
               else {
                  normalizedParameter += value.substr(1);
               }
               ++i;
            }
            else if (!shortOpts.empty()) {
               arg = '-' + shortOpts;
               haveArg = true;
            }
            else {
               ++i;
            }
            args.push_back(normalizedParameter);
         }
         return args;

      }

      ParsedArgs Parameters::parse(size_t argc, const char** argv, size_t offset) const 
      {
         ::std::vector< ::std::string> args = normalizeArguments(argc, argv, offset);
         ::std::map< ParameterPtr, ::std::vector< ParameterValuePtr>> parameters;
         ::std::vector< ::std::string> freeArgs;

         ::std::string parameter, shortOpts;

         auto i = args.begin();
         for (; i != args.end(); ++i) {
            if (*i == "--") {
               ++i;
               break;
            }
            ::std::string value;
            ParameterPtr opt = findParameter(*i, parameter, value, shortOpts);
            assert(shortOpts.empty());
            if (opt) {
               const ParameterValueConstructor& ctor = _parameters.find(opt)->second;
               ParameterValuePtr optarg;
               if (value.empty()) {
                  // just push back an empty value; we use it to count
                  // the number of occurrences of the parameter
                  optarg = ctor(*opt, nullptr);
               }
               else {
                  optarg = ctor(*opt, value.substr(1).c_str());
               }
               parameters[opt].push_back(optarg);
            }
            else {
               freeArgs.push_back(*i);
            }
         }
         while (i != args.end()) {
            freeArgs.push_back(*i);
            ++i;
         }

         for (auto k = _parameters.begin(); k != _parameters.end(); ++k) {
            auto opt = k->first;
            if (opt->isRequired() && parameters.find(opt) == parameters.end()) {
               throw ::std::invalid_argument("Missing required parameter " + opt->name());
            }
         }

         return ParsedArgs(::std::move(parameters), ::std::move(freeArgs));
      }
   }
}
