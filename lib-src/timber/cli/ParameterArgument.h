#ifndef _TIMBER_CLI_PARAMETERARGUMENT_H
#define _TIMBER_CLI_PARAMETERARGUMENT_H

#ifndef _TIMBER_CLI_PARAMETERARGUMENTBASE_H
#include <timber/cli/ParameterArgumentBase.h>
#endif

namespace timber {
   namespace cli {

      /**
       * A class to encapsulated a simple valye.
       */
      template<class T>
      class ParameterArgument : public ParameterArgumentBase
      {
            /**
             * Create a new parameter argument.
             * @param t the parameter value
             */
         public:
            ParameterArgument(const T& t) throw()
                  : _value(t)
            {
            }

            /** Destructor */
         public:
            ~ParameterArgument()
            {
            }

            /**
             * Get the value
             * @return the value
             */
         public:
            inline const T& value() const throw()
            {
               return _value;
            }

         public:
            const T _value;
      };

   }
}
#endif
