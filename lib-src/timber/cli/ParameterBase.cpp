#include <timber/cli/ParameterBase.h>
#include <iostream>
#include <stdexcept>
#include <codecvt>
#include <locale>

namespace timber {
   namespace cli {
      namespace {
         static bool isSingleChar(const ::std::string& utf8)
         {
            std::wstring_convert< std::codecvt_utf8< char32_t>, char32_t> ucs2conv;
            try {
               auto utf32 = ucs2conv.from_bytes(utf8);
               return utf32.length() == 1;
            }
            catch (const ::std::range_error&) {
               // the conversion failed
               return false;
            }
         }
      }
      ParameterBase::ParameterBase(const ParameterBase& opt)
            : _shortOpt(opt._shortOpt), _longOpt(opt._longOpt), _description(opt._description), _required(opt._required)
      {
      }

      ParameterBase::ParameterBase(char const* sOpt, char const* lOpt, const char* desc, bool required)
            : _required(required)
      {
         if (sOpt != nullptr) {
            _shortOpt = sOpt;
            if (_shortOpt.find_first_of("=-") != ::std::string::npos) {
               throw ::std::invalid_argument("Short parameter must not contain =-");
            }
            if (!_shortOpt.empty() && !isSingleChar(_shortOpt)) {
               throw ::std::invalid_argument("Short parameter contains more than one unicode codepoint");
            }
         }

         if (lOpt != nullptr) {
            _longOpt = lOpt;
            if (_longOpt.find_first_of("=-") != ::std::string::npos) {
               throw ::std::invalid_argument("Long parameter must not contain any of =-");
            }
         }

         if (_longOpt.empty() && _shortOpt.empty()) {
            throw ::std::invalid_argument("One of short and long parameter is required");
         }

         if (desc != nullptr) {
            _description = desc;
         }
      }
      ParameterBase::~ParameterBase()
      {
      }

      ::std::string ParameterBase::name() const
      {
         return _longOpt.empty() ? "-" + _shortOpt : "--" + _longOpt;
      }

      const ::std::string& ParameterBase::shortParameter() const
      {
         return _shortOpt;
      }

      const ::std::string& ParameterBase::longParameter() const
      {
         return _longOpt;
      }

      bool ParameterBase::isRequired() const
      {
         return _required;
      }

      const ::std::string& ParameterBase::description() const
      {
         return _description;
      }

   }
}
